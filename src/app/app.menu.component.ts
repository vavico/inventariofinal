import {Component, Input, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import {MenuItem} from 'primeng/primeng';
import {AppComponent} from './app.component';

@Component({
    selector: 'app-menu',
    template: `
        <div class="menu">
            <ul app-submenu [item]="model" root="true" parentActive="true"></ul>
        </div>
    `
})
export class AppMenuComponent implements OnInit {

    model: MenuItem[];

    ngOnInit() {
        this.model = [
            {label: 'Inicio', icon: 'fa fa-fw fa-event', routerLink: ['Inicio']},
            {
                label: 'Articulos', icon: 'fa fa-fw fa-bookmark', badgeStyleClass: 'green-badge',
                items: [
                    {label: 'Ingresar articulo', icon: 'fa fa-fw fa-code', routerLink:'articulo/guardar'},
                    {label: 'Listar articulos', icon: 'fa fa-fw fa-minus-square-o', routerLink:'articulo/listar'},
                    {label: 'Depreciación Manual', routerLink:'articulo/depreciacion'}
                ]
            },
            {
                label: 'Asignación', icon: 'fa fa-fw fa-list-alt',
                items: [
                    {label: 'Nueva Asignación', icon: 'fa fa-fw fa-table', routerLink:'asignar/guardar'},
                    {label: 'Ver Asignaciones', icon: 'fa fa-fw fa-columns', routerLink:'asignar/listar'}
                ]
            },
            {
                label: 'Ubicaciones', icon: 'fa fa-fw fa-building-o', badgeStyleClass: 'green-badge',
                items: [
                    {label: 'Ingresar Ubicación', icon: 'fa fa-fw fa-code', routerLink:'bodega/guardar'},
                    {label: 'Listar Ubicaciones', icon: 'fa fa-fw fa-minus-square-o', routerLink:'bodega/listar'}
                ]
            },
            
            {
                label: 'Categorias', icon: 'fa fa-fw fa-gg',
                items: [
                    {label: 'Ingresar Categoria',icon: 'fa fa-fw fa-code', routerLink:'categoria/guardar'},
                    {label: 'Listar Categorias', icon: 'fa fa-fw fa-minus-square-o',routerLink:'categoria/listar'},
                ]
            },
            
            {
                label: 'Docentes', icon: 'fa fa-fw fa-user', badgeStyleClass: 'green-badge',
                items: [
                    {label: 'Listar Docentes', icon: 'fa fa-fw fa-minus-square-o', routerLink:'docente/listar'}
                ]
            },
            {
                label: 'Instituciones', icon: 'fa fa-fw fa-home', badgeStyleClass: 'green-badge',
                items: [
                    {label: 'Ingresar Institución', icon: 'fa fa-fw fa-code', routerLink:'institucion/ingresar'},
                    {label: 'Listar Instituciones', icon: 'fa fa-fw fa-minus-square-o', routerLink:'institucion/listar'}
                ]
            },
            {
                label: 'Reportes', icon: 'fa fa-fw fa-print', badgeStyleClass: 'green-badge',
                items: [
                    {label: 'Reporte Artículos', icon: 'fa fa-fw fa-minus-square-o', routerLink:'reporte/articulo'}
                ]
            },
            {label: 'Soporte', icon: 'fa fa-fw fa-book', routerLink: 'Soporte'}
        ];
    }
}

@Component({
    /* tslint:disable:component-selector */
    selector: '[app-submenu]',
    /* tslint:enable:component-selector */
    template: `
        <ul>
            <ng-template ngFor let-child let-i="index" [ngForOf]="(root ? item : item.items)">
                <li [ngClass]="{'active-menuitem': isActive(i), 'ui-state-disabled':child.disabled}" [class]="child.badgeStyleClass">
                    <a *ngIf="!child.routerLink" [href]="child.url||'#'" (click)="itemClick($event,child,i)"
                       [attr.tabindex]="!visible ? '-1' : null"  [attr.target]="child.target">
                        <i [ngClass]="child.icon"></i>
                        <span>{{child.label}}</span>
                        <span class="menuitem-badge" *ngIf="child.badge">{{child.badge}}</span>
                        <i class="fa fa-fw fa-angle-down" *ngIf="child.items"></i>
                    </a>
                    <a *ngIf="child.routerLink" (click)="itemClick($event,child,i)" [attr.target]="child.target"
                        [routerLink]="!child.disabled?child.routerLink:null" routerLinkActive="active-menuitem-routerlink"
                       [routerLinkActiveOptions]="{exact: true}">
                        <i [ngClass]="child.icon"></i>
                        <span>{{child.label}}</span>
                        <span class="menuitem-badge" *ngIf="child.badge">{{child.badge}}</span>
                        <i class="fa fa-fw fa-angle-down" *ngIf="child.items"></i>
                    </a>
                    <ul app-submenu [item]="child" *ngIf="child.items"
                        [@children]="isActive(i) ? 'visible' : 'hidden'"  [parentActive]="isActive(i)"></ul>
                </li>
            </ng-template>
        </ul>
    `,
    animations: [
        trigger('children', [
            state('hidden', style({
                height: '0px'
            })),
            state('visible', style({
                height: '*'
            })),
            transition('visible => hidden', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
            transition('hidden => visible', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
        ])
    ]
})
export class AppSubMenuComponent {

    @Input() item: MenuItem;

    @Input() root: boolean;

    @Input() visible: boolean;

    activeIndex: number;

    _parentActive: boolean;

    constructor(public app: AppComponent) {}

    itemClick(event: Event, item: MenuItem, index: number) {
        if (item.disabled) {
            event.preventDefault();
            return;
        }

        this.activeIndex = (this.activeIndex === index) ? null : index;

        // execute command
        if (item.command) {
            item.command({originalEvent: event, item});
        }

        // prevent hash change
        if (item.items || (!item.url && !item.routerLink)) {
            //setTimeout(() => {this.app.scrollerViewChild.moveBar(); }, 400);
            event.preventDefault();
        }

        if (!item.items) {
            this.app.menuActiveMobile = false;
        }
    }

    isActive(index: number): boolean {
        return this.activeIndex === index;
    }

    @Input() get parentActive(): boolean {
        return this._parentActive;
    }

    set parentActive(val: boolean) {
        this._parentActive = val;

        if (!this._parentActive) {
            this.activeIndex = null;
        }
    }
}
