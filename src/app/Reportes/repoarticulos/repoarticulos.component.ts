import { Component, OnInit } from '@angular/core';
import { Articulo } from 'src/app/articulo/articulo';
import { ArticuloService } from 'src/app/articulo/articulo.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FilterUtils } from 'primeng/api';
import { BodegasService } from 'src/app/bodega/bodegas.service';
import { bodega } from 'src/app/bodega/bodega';

@Component({
  selector: 'app-repoarticulos',
  templateUrl: './repoarticulos.component.html',
  styleUrls: ['./repoarticulos.component.css']
})
export class RepoarticulosComponent implements OnInit {
  
  //Listar articulo
  articulos = new Array<Articulo>();
  codigo: string;
  articuloEditar: Articulo;
  selectedArtic: any[];

  bodegas = new Array<bodega>();
  selectedBodega: bodega;
  bandera = false;
  cols: any[];

  parametro: string;
  loading = true;
  totalrecords;
  numerofilas = 20;
  pagina = 0;
  bandera1=false;
  
  dia: number;
  mes: string;
  mm: number;
  aa: number;
  fecha: string;
  hoy = new Date();


  constructor(private artService: ArticuloService, private bodService: BodegasService, private router: Router) { 
    this.bodService.listarCategoria().subscribe(data => {
      this.bodegas = data;
    });
  }

  ngOnInit() {
    this.cols = [
      { field: 'art_codigo', header: 'Código'},
      { field: 'art_bien', header: 'Bien' },
      { field: 'art_serie', header: 'Serie' },
      { field: 'art_aula', header: 'Ubicación' },
      { field: 'art_asignado', header: 'Asignado' },
    ];
    
    FilterUtils['custom'] = (value, filter): boolean => {
      if (filter === undefined || filter === null || filter.trim() === '') {
          return true;
      }

      if (value === undefined || value === null) {
          return false;
      }
      
      return parseInt(filter) > value;
    }
    this.selectedArtic=this.cols;

    this.dia = this.hoy.getDate();
    this.mm = this.hoy.getMonth() + 1;
    this.aa = this.hoy.getFullYear();
    this.fecha = this.dia + "/" + this.mm + "/" + this.aa;
    if (this.mm == 1) {
      this.mes = "enero";
    }
    if (this.mm == 2) {
      this.mes = "febrero";
    }
    if (this.mm == 3) {
      this.mes = "marzo";
    }
    if (this.mm == 4) {
      this.mes = "abril";
    }
    if (this.mm == 5) {
      this.mes = "mayo";
    }
    if (this.mm == 6) {
      this.mes = "junio";
    }
    if (this.mm == 7) {
      this.mes = "julio";
    }
    if (this.mm == 8) {
      this.mes = "agosto";
    }
    if (this.mm == 9) {
      this.mes = "septiembre";
    }
    if (this.mm == 10) {
      this.mes = "octubre";
    }
    if (this.mm == 11) {
      this.mes = "noviembre";
    }
    if (this.mm == 12) {
      this.mes = "diciembre";
    }
    
  }

  
  buscarporParametro() { // Funciona
    if (this.selectedBodega != null){
      this.parametro = this.selectedBodega.aula_nombre;
    }
    var param = this.parametro.toUpperCase();
    this.artService.buscarporParametro(param).subscribe(data => {
      this.articulos = data;
      this.bandera1=true;
    });
  }

  buscarporCodigo() { // Funciona
    this.artService.buscarporCodigo(this.codigo).subscribe(data => {
      this.articulos = data;
    });
  }

}
