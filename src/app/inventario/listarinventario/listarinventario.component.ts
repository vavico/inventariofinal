import { Component, OnInit } from '@angular/core';
import { Inventario } from '../inventario';
import { InventarioService} from '../inventario.service';
import { ArticuloService} from '../articulo.service';
import { Router } from '@angular/router';
import { Articulo } from '../../articulo/articulo';
import { DocenteService } from '../docente.service'
import { Docente } from 'src/app/docente/docente';
@Component({
  selector: 'app-listarinventario',
  templateUrl: './listarinventario.component.html',
  styleUrls: ['./listarinventario.component.css']
})
export class ListarinventarioComponent implements OnInit {

  //Listar docentes
  inventarios = new Array<Inventario>();
  parametro: string;
  bandera= false;
  art: Articulo;
  cols: any[];
  selectedInventario: Inventario;
  constructor(private invService: InventarioService, private artService: ArticuloService, private router: Router) {  }

  ngOnInit() {
    this.listarInventario();
  
  }
  listarInventario(){ // Funciona
    
    this.invService.listarInventario().subscribe(data =>{
      this.inventarios = data;
    });
  }
  
  buscarporParametro(){ // Funciona
    this.invService.buscarporParametro(this.parametro).subscribe(data =>{
    this.inventarios = data;
    });
  }

  eliminarporId(inv){ // Funciona

    this.artService.editarporCodigo("NO ASIGNADO", inv.inv_art_codigo)
    .subscribe(data => {
      this.invService.eliminarporId(inv.inv_id).subscribe(data =>{
        this.listarInventario();
      });

    });
  }

}