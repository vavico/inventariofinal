import { Injectable } from '@angular/core';
import { Inventario } from './inventario';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InventarioService {

  private url: string = environment.URL_INVENTARIO + "/inventario";

  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor( private http: HttpClient ) { }

  // Guardar ok usando
  crearInventario(inv: Inventario): Observable<Inventario>{
    return this.http.post<Inventario>(`${this.url}/guardar`,inv);
  }

  // Listar ok - usando
  listarInventario(): Observable<Inventario[]>{
    return this.http.get<Inventario[]>(`${this.url}/listar`);
  }

  // Eliminar por id - usando
  eliminarporId(id:number) {
    return this.http.delete(`${this.url}/eliminar/${id}`);
  }

  // Buscar por parametro - usando
  buscarporParametro(parametro:String): Observable<Inventario[]>{
    return this.http.get<Inventario[]>(`${this.url}/listar/${parametro}`);
  }
  
  // Buscar por parametro - usando
  buscarporCedula(parametro:String): Observable<Inventario[]>{
    return this.http.get<Inventario[]>(`${this.url}/listar/cedula/${parametro}`);
  }
}
