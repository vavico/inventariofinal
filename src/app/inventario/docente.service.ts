import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Docente } from './../docente/docente';

import { GenericService }from "src/app/spring-generic-mvc/service/generic.service";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DocenteService extends GenericService<Docente>{

  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private http: HttpClient) { 
    super(
      http,
      environment.URL_DOCENTE,
      'docentes'
    )
  }

  // Listar ok usando
  listarDocente(): Observable<Docente[]> {
    return this.http.get<Docente[]>(`${environment.URL_DOCENTE}/docentes/listar`);
  }

  // Buscar por parametro - ok usando
  buscarporParametro(parametro: string): Observable<Docente[]> {
    return this.http.get<Docente[]>(`${environment.URL_DOCENTE}/docentes/listar/${parametro}`);
  }

}