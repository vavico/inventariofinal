import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Articulo } from './../articulo/articulo';
import { retry, catchError } from 'rxjs/operators';

import { GenericService }from "src/app/spring-generic-mvc/service/generic.service";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ArticuloService extends GenericService<Articulo> {

  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor( private http: HttpClient ) {
    super(
      http,
      environment.URL_INVENTARIO,
      'articulo'
    )
   }

  editarporCodigo(data, codigo){ // usando
    return this.http.put<string>(`${environment.URL_INVENTARIO}/articulo/editar/codigo/${codigo}`, data, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    )
  }

  // Buscar por codigo - usando
  buscarporCodigo(codigo:string): Observable<Articulo[]>{
    return this.http.get<Articulo[]>(`${environment.URL_INVENTARIO}/articulo/listar/${codigo}`);
  }

   // Buscar por codigo - usando
   buscarporAula(aula:string): Observable<Articulo[]>{
    return this.http.get<Articulo[]>(`${environment.URL_INVENTARIO}/articulo/listar/${aula}`);
  }

  // Ver asignados - usando
  verAsignado(asignado:string): Observable<Articulo[]>{
    return this.http.get<Articulo[]>(`${environment.URL_INVENTARIO}/articulo/listar/asignado/${asignado}`);
  }

  // Error handling - ok
  errorHandl(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
 }
    
}
