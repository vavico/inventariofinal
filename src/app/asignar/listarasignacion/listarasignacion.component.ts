import { Component, OnInit } from '@angular/core';
import { Articulo } from 'src/app/articulo/articulo';
import { Asignacion } from '../asignacion';
import { AsignacionesService } from '../asignaciones.service';
import { ArticuloService } from 'src/app/articulo/articulo.service';
import { Router } from '@angular/router';
import { DocenteService } from 'src/app/docente/docente.service';
import { Docente } from 'src/app/docente/docente';
import { bodega } from 'src/app/bodega/bodega';
import { BodegasService } from 'src/app/bodega/bodegas.service';

@Component({
  selector: 'app-listarasignacion',
  templateUrl: './listarasignacion.component.html',
  styleUrls: ['./listarasignacion.component.css']
})
export class ListarasignacionComponent implements OnInit {
//Listar docentes
  docentes = new Array<Docente>();
  bodegas = new Array<bodega>();
  parametro: string;
  inventarios = new Array<Asignacion>();
  bandera= false;
  art: Articulo;
  cols: any[];
  selectedDocente: Docente;
  selectedBodega: bodega;
  selectedInventario: Asignacion;
  constructor(private invService: AsignacionesService, private docService:DocenteService, private bodService: BodegasService, private artService: ArticuloService, private router: Router) {  
    this.docService.listarDoc().subscribe(data3 => {
      this.docentes = data3;
    });
    this.bodService.listarCategoria().subscribe(data => {
      this.bodegas = data;
    });
  }

  ngOnInit() {
    this.listarInventario();

  }
  listarInventario(){ // Funciona
    
    this.invService.listarInventario().subscribe(data =>{
      this.inventarios = data;
    });
  }

  buscarporParametro(){ // Funciona
    this.inventarios =[];
    if (this.selectedBodega != null){
      this.parametro = this.selectedBodega.aula_nombre;
    }
    if (this.selectedDocente != null){
      this.parametro = this.selectedDocente.per_identificacion;
    }
    if (this.parametro.length>0){
      var param = this.parametro.toUpperCase();
      this.invService.buscarporParametro(param).subscribe(data =>{
        this.inventarios = data;
        if (this.inventarios.length==0){
          alert("Parámetro no asignado");
        }
        else{
          this.bandera=true;
        }
      });
    }
    else{
      alert("Parámetro no ingresado");
    }
  }

  buscarporParam(){ // Funciona
    if (this.selectedBodega != null){
      this.parametro = this.selectedBodega.aula_nombre;
    }
    if (this.selectedDocente != null){
      this.parametro = this.selectedDocente.per_identificacion;
    }
    this.invService.buscarporParametro(this.parametro).subscribe(data =>{
      this.inventarios = data;
    });
  }

  eliminarporParametro(){ // Funciona
    for (let inv of this.inventarios){
      this.artService.editarporId("NO ASIGNADO", inv.inv_art_codigo)
      .subscribe(data => {
        this.invService.eliminarporId(inv.inv_id).subscribe(data =>{
          this.buscarporParam();
        });
      });
    }
    
  }

  eliminarporId(inv){ // Funciona

    this.artService.editarporId("NO ASIGNADO", inv.inv_art_codigo)
    .subscribe(data => {
      this.invService.eliminarporId(inv.inv_id).subscribe(data =>{
        this.buscarporParametro();
      });

    });
  }
  editarporId() {
    localStorage.setItem('inventarioEdit',JSON.stringify(this.inventarios));
    this.router.navigate(['/asignar/modificar']);
  }
}