import { Component, OnInit } from '@angular/core';
import { Docente } from 'src/app/docente/docente';
import { bodega } from 'src/app/bodega/bodega';
import { Articulo } from 'src/app/articulo/articulo';
import { Asignacion } from '../asignacion';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AsignacionesService } from '../asignaciones.service';
import { BodegasService } from 'src/app/bodega/bodegas.service';
import { DocenteService } from 'src/app/docente/docente.service';
import { ArticuloService } from 'src/app/articulo/articulo.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modificarasignacion',
  templateUrl: './modificarasignacion.component.html',
  styleUrls: ['./modificarasignacion.component.css']
})
export class ModificarasignacionComponent implements OnInit {
  
  inv: Asignacion;
  docentes = new Array<Docente>();
  bodegas = new Array<bodega>();
  parametro: string;
  dia: number;
  mes: string;
  mm: number;
  aa: number;
  fecha: string;
  hoy = new Date();

  cols: any[];
  bandera1 = true;
  bandera2 = false;
  bandera3 = false;
  bandter = true;
  activo = false;

  mensaje: string;

  ceduladoc: string;
  nombredoc: string;
  apellidodoc: string;
  doc: Docente;
  art;

  selectedArticulo: Articulo;
  selectedDocente: Docente;
  selectedBodega: bodega;
  selectedInventario: Asignacion;
  inventario = new Array<Asignacion>();

  //Listar articulo
  articulos = new Array<Articulo>();

  // Ingresar inventario
  addForm: FormGroup;
  submitted = false;

  //Listar inventario
  inventarios = new Array<Asignacion>();

  sourceArts = new Array<Articulo>();
  targetArts = new Array<Articulo>();

  artic: Array<Articulo>;
  aux: string;

  constructor(private invService: AsignacionesService, private bodService: BodegasService, private docService: DocenteService, private artService: ArticuloService, private router: Router, private formBuilder: FormBuilder) {
    this.inventarios = JSON.parse( localStorage.getItem("inventarioEdit"));
    this.docService.listarDoc().subscribe(data3 => {
      this.docentes = data3;
    });
    this.bodService.listarCategoria().subscribe(data => {
      this.bodegas = data;
    });
  }

  ngOnInit() {
    this.sourceArts = [];
    this.targetArts = [];
    for (let inv of this.inventarios){
      this.artService.buscarporCod(inv.inv_art_codigo).subscribe(data => {
        this.sourceArts.push(data);
      });
    }
    this.selectedDocente = {
      per_identificacion : this.inventarios[0].inv_doc_cedula,
      per_fechanacimiento : "",
      per_celular : "",
      per_correo : "",
      per_primerapellido : this.inventarios[0].inv_doc_apellido,
      per_primernombre : this.inventarios[0].inv_doc_nombre,
      per_segundonombre : "",
    }
    this.dia = this.hoy.getDate();
    this.mm = this.hoy.getMonth() + 1;
    this.aa = this.hoy.getFullYear();
    this.fecha = this.dia + "/" + this.mm + "/" + this.aa;
    if (this.mm == 1) {
      this.mes = "enero";
    }
    if (this.mm == 2) {
      this.mes = "febrero";
    }
    if (this.mm == 3) {
      this.mes = "marzo";
    }
    if (this.mm == 4) {
      this.mes = "abril";
    }
    if (this.mm == 5) {
      this.mes = "mayo";
    }
    if (this.mm == 6) {
      this.mes = "junio";
    }
    if (this.mm == 7) {
      this.mes = "julio";
    }
    if (this.mm == 8) {
      this.mes = "agosto";
    }
    if (this.mm == 9) {
      this.mes = "septiembre";
    }
    if (this.mm == 10) {
      this.mes = "octubre";
    }
    if (this.mm == 11) {
      this.mes = "noviembre";
    }
    if (this.mm == 12) {
      this.mes = "diciembre";
    }
  }

  buscarporArticulo() { // Funciona
    if (this.selectedBodega != null){
      this.parametro = this.selectedBodega.aula_nombre;
    }
    var param = this.parametro.toUpperCase();
    this.artService.buscarporParametro(param).subscribe(data => {
      this.sourceArts = data;
    });
  }

  buscarporArt() { // Funciona
    this.parametro= this.selectedBodega.aula_nombre;
    this.artService.buscarporParametro(this.parametro).subscribe(data => {
      this.sourceArts = data;
    });
  }

  buscarporParametro() { // Funciona -ok
    this.docService.buscarporParametro(this.parametro).subscribe(data => {
      this.docentes = data;
    });
  }

  seleccionarDocente() {
    this.ceduladoc = this.selectedDocente.per_identificacion;
    this.nombredoc = this.selectedDocente.per_primernombre;
    this.apellidodoc = this.selectedDocente.per_primerapellido;
  }

  seleccionarArticulo() {
    this.inventario=[];
    for (let art of this.targetArts) {
      var param = this.nombredoc + " " + this.apellidodoc;
      this.artService.editarporId(param, art.art_codigo)
      .subscribe(data => {
      });
        //Ingresa inventario
      this.inventario.push({
        inv_id: this.inventarios.length,
        inv_doc_cedula: this.ceduladoc,
        inv_doc_nombre: this.nombredoc,
        inv_doc_apellido: this.apellidodoc,
        inv_art_codigo: art.art_codigo,
        inv_art_bien: art.art_bien,
        inv_art_color: art.art_color,
        inv_art_marca: art.art_marca,
        inv_art_modelo: art.art_modelo,
        inv_art_serie: art.art_serie,
        inv_estado: art.art_estado,
        inv_art_ubicacion_destino: art.art_aula,
        inv_fecha: this.fecha,
      });
    }
    for (let inv of this.inventario){
      this.invService.editarporCodigo(inv, inv.inv_art_codigo)
        .subscribe(data => {
      });
    }
  }

  terminarRegistro() {
    this.bandera1 = false;
    this.bandera2 = false;
    this.bandera3 = true;
    this.activo = true;
    this.bandter = false;
    this.seleccionarDocente();
    this.seleccionarArticulo();
  }

  rootear() {
    this.router.navigate(['/asignar/listar']);
  }
}
