import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Asignacion } from './asignacion';
import { Observable, throwError } from 'rxjs';
import { Articulo } from '../articulo/articulo';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AsignacionesService {

  private url: string = environment.URL_INVENTARIO + "/inventario";

  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor( private http: HttpClient ) { }

  // Guardar ok usando
  crearInventario(inv: Asignacion): Observable<Asignacion>{
    return this.http.post<Asignacion>(`${this.url}/guardar`,inv);
  }

  crearInv(inv: Asignacion[]): Observable<Asignacion>{
    return this.http.post<Asignacion>(`${this.url}/guarda`,inv);
  }

  // Listar ok - usando
  listarInventario(): Observable<Asignacion[]>{
    return this.http.get<Asignacion[]>(`${this.url}/listar`);
  }

  // Eliminar por id - usando
  eliminarporId(id:number) {
    return this.http.delete(`${this.url}/eliminar/${id}`);
  }

  // Buscar por parametro - usando
  buscarporParametro(parametro:String): Observable<Asignacion[]>{
    return this.http.get<Asignacion[]>(`${this.url}/listar/${parametro}`);
  }
  
  // Buscar por parametro - usando
  buscarporCedula(parametro:string): Observable<Asignacion[]>{
    return this.http.get<Asignacion[]>(`${this.url}/listar/cedula/${parametro}`);
  }

  editarporCodigo(data, codigo){ // usando
    return this.http.put<string>(`${this.url}/editar/${codigo}`, data, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    )
  }

  // Error handling
  errorHandl(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
 }

}
