export interface Asignacion{
    inv_id: number;
    inv_doc_cedula: string;
    inv_doc_nombre: string;
    inv_doc_apellido: string;
    inv_art_codigo: string;
    inv_art_bien: string;
    inv_art_color: string;
    inv_art_marca: string;
    inv_art_modelo: string;
    inv_art_serie: string
    inv_estado: string;
    inv_art_ubicacion_destino: string;
    inv_fecha: string;
}