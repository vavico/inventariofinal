import { Component, OnInit } from '@angular/core';
import { ArticuloService } from '../articulo.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CategoriasService} from '../../categoria/categorias.service';
import { InstitucionService} from '../../institucion/institucion.service';
import { BodegasService } from 'src/app/bodega/bodegas.service';
import { bodega } from 'src/app/bodega/bodega';
import { institucion } from 'src/app/institucion/institucion';
import { categoria } from 'src/app/categoria/categoria';
import { DocenteService } from 'src/app/docente/docente.service';
import { Docente } from 'src/app/docente/docente';

@Component({
  selector: 'app-registrararticulo',
  templateUrl: './registrararticulo.component.html',
  styleUrls: ['./registrararticulo.component.css', ]
})
export class RegistrararticuloComponent implements OnInit {
 
  date1: Date;
  date2: Date;
  fec1= this.date1;
  fec2= this.date2;
  dis: boolean;
  dis2: boolean;
  bodegas: bodega[];
  selectedBodega = {};
  empresas: institucion[];
  selectedEmpresa={};
  categorias: categoria[];
  selectedCategoria ={};
  docentes: Docente[]=[];
  selectedDocente = {};
  asg: Docente;

  showDialog() {
    this.dis = true;
  }
  showDialog2() {
    this.dis2 = true;
  }
  
  constructor(private artservice: ArticuloService, private docService: DocenteService, private bodService: BodegasService,  private router: Router, private formBuilder: FormBuilder, private catservice: CategoriasService, private empservice: InstitucionService) { 
    this.asg = {
      per_identificacion : "NO ASIGNADO",
      per_fechanacimiento : "NO ASIGNADO",
      per_celular : "NO ASIGNADO",
      per_correo : "NO ASIGNADO",
      per_primerapellido : "NO ASIGNADO",
      per_primernombre : "NO ASIGNADO",
      per_segundonombre : "NO ASIGNADO",
    }
    this.bodService.listarCategoria().subscribe(data => {
      this.bodegas = data;
    });
    this.empservice.listarEmpresa().subscribe(data2 => {
      this.empresas = data2;
    });
    this.catservice.listarCategoria().subscribe(data1 => {
      this.categorias = data1;
    });
    this.docService.listarDoc().subscribe(data3 => {
      this.docentes = data3;
      this.docentes.push (this.asg);
    });
    this.fec1 = new Date();
    this.fec2 = new Date();
  }
  addForm: FormGroup;
  addForm1: FormGroup;
  addForm2: FormGroup;
  submitted = false;
  submitted1 = false;
  submitted2 = false;
  
  
  ngOnInit() {
    this.addForm1 = this.formBuilder.group({
      cat_nombre: [],
      cat_val_depre: ['', Validators.required],
      cat_tiempo_depre: ['', Validators.required]
     
    });
    this.addForm2 = this.formBuilder.group({
      emp_nombre: ['', Validators.required]
     
    });
    this.addForm = this.formBuilder.group({
      art_id: [],
      art_codigo: ['', Validators.required],
      art_cod_aterior: ['', Validators.required],
      art_num_acta: ['', Validators.required],
      art_bld: ['', Validators.required],
      art_bien: ['', Validators.required],
      art_serie: ['', Validators.required],
      art_modelo: ['', Validators.required],
      art_marca: ['', Validators.required],
      art_color: ['', Validators.required],
      art_material: ['', Validators.required],
      art_dimensiones: ['', Validators.required],
      art_edificio: ['', Validators.required],
      art_aula: ['', Validators.required],
      art_cuenta_contable: ['', Validators.required],
      art_fecha_ing: ['', Validators.required],
      art_fecha_depre: ['', Validators.required],
      art_valor_contable: ['', Validators.required],
      art_observacion: ['', Validators.required],
      art_conciliacion: ['', Validators.required],
      art_estado: ['', Validators.required],
      art_categoria: ['', Validators.required],
      art_precio: ['', Validators.required],
      art_empresa: ['', Validators.required],
      art_asignado: ['', Validators.required],
      art_descripcion: ['', Validators.required],
      art_bodega: ['', Validators.required],


    });

  }
  get f() { return this.addForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.addForm.invalid) {
      return;
    }
    this.artservice.crearArticulo(this.addForm.value)
      .subscribe(data => {
        alert("Artículo guardado con éxito");
        this.router.navigate(['/articulo/listar']);
      });

  }

  onSubmitCategoria(){
    this.submitted1 = true;
    if (this.addForm1.invalid){
      return;
    }
    this.catservice.crearCategoria(this.addForm1.value)
    .subscribe(data => {
      alert("Artículo guardado con éxito");
    });

  }
  onSubmitInst(){
    this.submitted2 = true;
    if (this.addForm2.invalid){
      return;
    }
    this.empservice.crearEmpresa(this.addForm2.value).subscribe(data => {
      alert("Empresa guardado con éxito");
    });

  }

}

