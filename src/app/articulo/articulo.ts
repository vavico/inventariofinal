export interface Articulo{
    art_id : number;
    art_codigo : string;
    art_cod_anterior : string;
    art_numero_acta : string;
    art_bld : string;
    art_bien : string;
    art_serie : string;
    art_modelo : string;
    art_marca : string;
    art_color : string;
    art_material : string;
    art_descripcion: string;
    art_dimensiones : string;
    art_aula : string;
    art_cuenta_contable : string;
    art_fecha_ing : Date;
    art_fecha_depre : Date;
    art_valor_contable : string;
    art_observacion : string;
    art_conciliacion : string;
    art_estado : string;
    art_categoria : string;
    art_empresa : string;
    art_asignado : string;
}

