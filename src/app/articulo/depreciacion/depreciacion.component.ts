import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-depreciacion',
  templateUrl: './depreciacion.component.html',
  styleUrls: ['./depreciacion.component.css']
})
export class DepreciacionComponent implements OnInit {

  bandera = true;
  hoy= new Date();
  dia: number;
  mm: number;
  fecha: string;
  constructor() { }

  ngOnInit() {
    this.dia = this.hoy.getDate();
    this.mm = this.hoy.getMonth() + 1;
    this.fecha = this.dia + "/" + this.mm;
    if (this.fecha == '31/12'){
      this.bandera = false;
    }
  }

  onSubmit() {
    
     //this.submitted = true;
    
    // if (this.addForm.invalid) {
    //   return;
    // }
    // this.artservice.crearArticulo(this.addForm.value)
    //   .subscribe(data => {
    //     alert("Artículo guardado con éxito");
    //     this.router.navigate(['/articulo/listar']);
    //   });

  }
}
