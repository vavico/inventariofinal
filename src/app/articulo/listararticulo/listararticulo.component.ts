import { Component, OnInit } from '@angular/core';
import { Articulo } from '../articulo';
import { ArticuloService } from '../articulo.service';
import { Router } from '@angular/router';
import { QueryOptions } from 'src/app/spring-generic-mvc/model';
import { FilterUtils } from 'primeng/components/utils/filterutils';


@Component({
  selector: 'app-listararticulo',
  templateUrl: './listararticulo.component.html',
  styleUrls: ['./listararticulo.component.css'],
})

export class ListararticuloComponent implements OnInit {

  //Listar articulo
  articulos = new Array<Articulo>();
  codigo: string;
  articuloEditar: Articulo;
  selectedArtic: any[];

  bandera = false;
  cols: any[];

  loading = true;
  totalrecords;
  numerofilas = 10;
  pagina = 0;

  constructor(private artService: ArticuloService, private router: Router) { }

  ngOnInit() {
    //this.listarArticulo();
    
    this.cols = [
      { field: 'art_codigo', header: 'Código'},
      { field: 'art_cod_anterior', header: 'Código Anterior' },
      { field: 'art_numero_acta', header: 'Num. Acta' },
      { field: 'art_bld', header: 'BLD/BCA' },
      { field: 'art_bien', header: 'Bien' },
      { field: 'art_serie', header: 'Serie' },
      { field: 'art_modelo', header: 'Modelo' },
      { field: 'art_marca', header: 'Marca' },
      { field: 'art_color', header: 'Color' },
      { field: 'art_material', header: 'Material' },
      { field: 'art_dimensiones', header: 'Dimensiones' },
      { field: 'art_aula', header: 'Aula' },
      { field: 'art_cuenta_contable', header: 'Cuenta Contable' },
      { field: 'art_fecha_ing', header: 'Fecha Ing.' },
      { field: 'art_fecha_depre', header: 'Fecha Ult. Depreciación' },
      { field: 'art_valor_contable', header: 'Valor Contable' },
      { field: 'art_observacion', header: 'Observación' },
      { field: 'art_conciliación', header: 'Conciliación' },
      { field: 'art_estado', header: 'Estado' },
      { field: 'art_categoria', header: 'Categoria' },
      { field: 'art_empresa', header: 'Empresa' },
      { field: 'art_asignado', header: 'Asignado' },
      
    ];
    
    FilterUtils['custom'] = (value, filter): boolean => {
      if (filter === undefined || filter === null || filter.trim() === '') {
          return true;
      }

      if (value === undefined || value === null) {
          return false;
      }
      
      return parseInt(filter) > value;
    }
    this.listar();
    this.selectedArtic=this.cols;

    
  }

  //eventos de load lazy

  loadCarsLazy(event) {
    this.loading = true;

    setTimeout(() => {
      if (this.articulos) {
        const q: QueryOptions = new QueryOptions(); //El archivo para paginar los articulos realizado en spring tools
        q.pageSize = event.rows;
        // Calculo de pagina
        let page = Math.ceil(event.first / this.numerofilas);
        this.pagina = page;
        if (page < 0) {
          page = 0;
        }
        q.pageNumber = page;
        // Sorts
        q.sortField = event.sortField;
        q.sortOrder = event.sortOrder;
        if (event.filters.global) {
          this.artService.find(q, event.filters.global.value).subscribe(resp => {
            this.articulos = resp.content;
            this.totalrecords = resp.totalElements*1;
          });
        } else {
          this.artService.list(q).subscribe(resp => {
            this.articulos = resp.content;
            this.totalrecords = resp.totalElements*1;
          });
        }

        this.loading = false;
      }
    }, 1000);
  }

  columnFilter(event: any) {
  }

  onRowSelect(e) {
  }

  listar() {
    const q: QueryOptions = new QueryOptions();
    q.pageSize = this.numerofilas;
    q.pageNumber = this.pagina;
    this.artService.list(q).subscribe(resp => {
      this.articulos = resp.content;
      this.totalrecords = resp.totalElements;
    });
  }

  buscarporCodigo() { // Funciona
    this.artService.buscarporCodigo(this.codigo).subscribe(data => {
      this.articulos = data;
    });
  }

  editarporId() {
      localStorage.setItem('articuloEdit',JSON.stringify(this.articuloEditar));
       this.router.navigate(['/articulo/editar']);
       this.bandera=true;
  }

  eliminarporId(art: Articulo) { // Funciona
    if (art.art_asignado == "ASIGNADO") {
      alert("El articulo se encuentra asignado, no se permite eliminar");
    } else {
      this.artService.eliminarporId(art.art_id).subscribe(data => {
        alert("Eliminado");
        this.listar();
      });
    }

  }

  volver() {
    this.bandera = false;
    this.listar();
  }

}