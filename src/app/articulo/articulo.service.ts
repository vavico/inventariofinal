import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import { Observable, throwError } from 'rxjs'
import { Articulo } from './articulo';
import { retry, catchError } from 'rxjs/operators';

import { GenericService }from "src/app/spring-generic-mvc/service/generic.service";
import { environment } from 'src/environments/environment.prod';

@Injectable()
export class ArticuloService extends GenericService<Articulo> {
  
  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  
  constructor(private http: HttpClient) {
    super(
      http,
      environment.URL_INVENTARIO,
      'articulo'
    )
  } 

  // Guardar articulo - ok
  crearArticulo(art: Articulo): Observable<Articulo>{
    return this.http.post<Articulo>(`${environment.URL_INVENTARIO}/articulo/guardar`,art);
  }

  getArticulo() {
    return this.http.get<any>(`${environment.URL_INVENTARIO}/articulo/listarart`)
      .toPromise()
      .then(res => <Articulo[]>res.data )
      .then(data => { return data; });
  }

  // Listar articulos - ok
  listarArticulo(): Observable<Articulo[]>{
    return this.http.get<Articulo[]>(`${environment.URL_INVENTARIO}/articulo/listar`);
  }

  listarArt(): Observable<Articulo[]>{
    return this.http.get<Array<Articulo>>(`${environment.URL_INVENTARIO}/articulo/listarart`);
  }

  editarporId(data, codigo){ // usando
    return this.http.put<string>(`${environment.URL_INVENTARIO}/articulo/editar/codigo/${codigo}`, data, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    )
  }

  // Eliminar por id - ok
  eliminarporId(id:number) {
    return this.http.delete(`${environment.URL_INVENTARIO}/articulo/eliminar/${id}`);
  }

  // Buscar por codigo - ok
  buscarporCodigo(codigo:string): Observable<Articulo[]>{
    return this.http.get<Articulo[]>(`${environment.URL_INVENTARIO}/articulo/listar/${codigo}`);
  }
  
  buscarporParametro(codigo:string): Observable<Articulo[]>{
    return this.http.get<Array<Articulo>>(`${environment.URL_INVENTARIO}/articulo/listart/${codigo}`);
  }
  // Buscar por aula - ok
  buscarporAula(aula:string): Observable<Articulo[]>{
    return this.http.get<Articulo[]>(`${environment.URL_INVENTARIO}/articulo/listar/${aula}`);
  }

  buscarporCod(codigo:string): Observable<Articulo>{
    return this.http.get<Articulo>(`${environment.URL_INVENTARIO}/articulo/listar/codigo/${codigo}`);
  }

  // Ver asignados
  verAsignado(asignado:string): Observable<Articulo[]>{
    return this.http.get<Articulo[]>(`${environment.URL_INVENTARIO}/articulo/listar/asignado/${asignado}`);
  }

  // Error handling
  errorHandl(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
 }
    
}
