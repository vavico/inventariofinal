import { Component,Input, OnInit } from '@angular/core';
import { Articulo } from '../articulo';
import { ArticuloService } from '../articulo.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { bodega } from 'src/app/bodega/bodega';
import { institucion } from 'src/app/institucion/institucion';
import { categoria } from 'src/app/categoria/categoria';
import { BodegasService } from 'src/app/bodega/bodegas.service';
import { CategoriasService } from 'src/app/categoria/categorias.service';
import { InstitucionService } from 'src/app/institucion/institucion.service';
import { Docente } from 'src/app/docente/docente';
import { DocenteService } from 'src/app/docente/docente.service';

@Component({
  selector: 'app-actualizararticulo',
  templateUrl: './actualizararticulo.component.html',
  styleUrls: ['./actualizararticulo.component.css']
})
export class ActualizararticuloComponent implements OnInit {

  art: Articulo;
  date1: Date;
  date2: Date;
  fec1= this.date1;
  fec2= this.date2;
  dis: boolean;
  dis2: boolean;
  bodegas: bodega[];
  selectedBodega = {};
  empresas: institucion[];
  selectedEmpresa={};
  categorias: categoria[];
  selectedCategoria ={};
  docentes: Docente[]=[];
  selectedDocente = {};
  asg: Docente;
  showDialog() {
    this.dis = true;
  }
  showDialog2() {
    this.dis2 = true;
  }
  
  constructor(private artservice: ArticuloService, private docService:DocenteService, private bodService: BodegasService,  private router: Router, private formBuilder: FormBuilder, private catservice: CategoriasService, private empservice: InstitucionService) { 
    this.asg = {
      per_identificacion : "NO ASIGNADO",
      per_fechanacimiento : "NO ASIGNADO",
      per_celular : "NO ASIGNADO",
      per_correo : "NO ASIGNADO",
      per_primerapellido : "NO ASIGNADO",
      per_primernombre : "NO ASIGNADO",
      per_segundonombre : "NO ASIGNADO",
    }
    this.art = JSON.parse( localStorage.getItem("articuloEdit"));
    this.bodService.listarCategoria().subscribe(data => {
      this.bodegas = data;
    });
    this.empservice.listarEmpresa().subscribe(data2 => {
      this.empresas = data2;
    });
    this.catservice.listarCategoria().subscribe(data1 => {
      this.categorias = data1;
    });
    this.docService.listarDoc().subscribe(data3 => {
      this.docentes = data3;
      this.docentes.push (this.asg);
    });
    this.fec1 = new Date();
    this.fec2 = new Date();
  }
  addForm: FormGroup;
  addForm1: FormGroup;
  addForm2: FormGroup;
  submitted = false;
  submitted1 = false;
  submitted2 = false;
  
  
  ngOnInit() {
    this.addForm1 = this.formBuilder.group({
      cat_nombre: [],
      cat_val_depre: ['', Validators.required],
      cat_tiempo_depre: ['', Validators.required]
     
    });
    this.addForm2 = this.formBuilder.group({
      emp_nombre: ['', Validators.required]
     
    });
    this.addForm = this.formBuilder.group({
      art_id: this.art.art_id,
      art_codigo: this.art.art_codigo,
      art_cod_aterior: this.art.art_cod_anterior,
      art_num_acta: this.art.art_numero_acta,
      art_bld: this.art.art_bld,
      art_bien: this.art.art_bien,
      art_serie: this.art.art_serie,
      art_modelo: this.art.art_modelo,
      art_marca: this.art.art_marca,
      art_color: this.art.art_color,
      art_material: this.art.art_material,
      art_dimensiones: this.art.art_dimensiones,
      art_aula: this.art.art_aula,
      art_cuenta_contable: this.art.art_cuenta_contable,
      art_fecha_ing: "",
      art_fecha_depre: "",
      art_valor_contable: this.art.art_valor_contable,
      art_observacion: this.art.art_observacion,
      art_conciliacion: this.art.art_conciliacion,
      art_estado: this.art.art_estado,
      art_categoria: this.art.art_categoria,
      art_precio: this.art.art_valor_contable,
      art_empresa: this.art.art_empresa,
      art_asignado: this.art.art_asignado,
      art_descripcion: this.art.art_descripcion,
      art_bodega: this.art.art_aula,


    });

  }
  get f() { return this.addForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.addForm.invalid) {
      return;
    }
    this.artservice.editarporId(this.addForm.value, this.art.art_id)
      .subscribe(data => {
        alert("Artículo guardado con éxito");
        this.router.navigate(['/articulo/listar']);
      });

  }

  onSubmitCategoria(){
    this.submitted1 = true;
    if (this.addForm1.invalid){
      return;
    }
    this.catservice.crearCategoria(this.addForm1.value)
    .subscribe(data => {
      alert("Artículo guardado con éxito");
    });

  }
  onSubmitInst(){
    this.submitted2 = true;
    if (this.addForm2.invalid){
      return;
    }
    this.empservice.crearEmpresa(this.addForm2.value).subscribe(data => {
      alert("Empresa guardado con éxito");
    });

  }
}