import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/components/common/menuitem';

@Component({
  selector: 'app-menubar',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor() { }
  items: MenuItem[];
  
  ngOnInit() {
    this.items = [

            {
                label: 'INVENTARIO',
                items: [
                    [
                        {
                            items: [
                            {label: 'Nuevo ingreso', routerLink:'asignar/guardar'},
                            {label: 'Ver registros', routerLink:'asignar/listar'},
                            ]
                        },
                        
                    ],
                    
                ]
            },
            {
                label: 'ADMINISTRAR ARTICULOS',
                items: [
                    [
                        {
                            items: [
                                {label: 'Ingresar articulo', routerLink:'articulo/guardar'},
                                {label: 'Listar articulos', routerLink:'articulo/listar'},
                            ]
                        },

                    ],
                    
                ]
            },          

            {
                label: 'ADMINISTRAR DOCENTES',
                items: [
                    [
                        {
                            items: [
                                {label: 'Registrar docente', routerLink:'docente/guardar'},
                                {label: 'Ver lista de docentes', routerLink:'docente/listar'},
                            ]
                        },

                    ],
                    
                ]
            },
          
            
      ];
  }
}
