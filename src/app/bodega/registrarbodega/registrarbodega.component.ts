import { Component, OnInit } from '@angular/core';
import { BodegasService } from '../bodegas.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { almacen } from '../bodega';



@Component({
  selector: 'app-registrarbodega',
  templateUrl: './registrarbodega.component.html',
  styleUrls: ['./registrarbodega.component.css']
})

export class RegistrarbodegaComponent implements OnInit {

  edificios= new Array<almacen>();
  selectedAlmacen: string;
  almacenes = new Array<string>();

  constructor(private bodservice: BodegasService, private router: Router, private formBuilder: FormBuilder) { 
    this.bodservice.listarEdificio().subscribe(data => {
      this.edificios = data;
      console.log(data);
      for (let alm of data){
        this.almacenes.push(alm.edif_nombre);
      }
      console.log(this.almacenes);
    });
    
  }
  addForm: FormGroup;
  submitted = false;



  ngOnInit() {
    
    this.addForm = this.formBuilder.group({
      aula_id : [],
      aula_nombre : ['',Validators.required],
      aula_edif_id : ['',Validators.required],
    });
  }
  get f(){return this.addForm.controls;}

  onSubmit(){
    this.submitted = true;
    if (this.addForm.invalid){
      return;
    }
    this.bodservice.crearCategoria(this.addForm.value)
    .subscribe(data => {
      alert("Bodega guardado con éxito");
      this.router.navigate(['/bodega/listar']);
    });

  }
}
