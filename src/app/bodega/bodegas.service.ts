import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { bodega, almacen } from './bodega';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BodegasService {
  private url: string = environment.URL_INVENTARIO + "/aula";

  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor( private http: HttpClient ) { }

  // Guardar articulo - ok
  crearCategoria(bod: bodega): Observable<bodega>{
    return this.http.post<bodega>(`${this.url}/guardar`, bod);
  }

  // Listar articulos - ok
  listarCategoria(): Observable<bodega[]>{
    return this.http.get<bodega[]>(`${this.url}/listar`);
  }

  listarEdificio(): Observable<almacen[]>{
    return this.http.get<almacen[]>(`${environment.URL_INVENTARIO}/edificio/listar`);
  }

  editarporId(data: bodega, id): Observable<bodega> {
    return this.http.put<bodega>(`${this.url}/editar/${id}`, JSON.stringify(data), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    )
  }

  // Eliminar por id - ok
  eliminarporId(id:number) {
    return this.http.delete(`${this.url}/eliminar/${id}`);
  }

  // Buscar por nombre - ok
  buscarporNombre(nombre:string): Observable<bodega[]>{
    return this.http.get<bodega[]>(`${this.url}/listar/${nombre}`);
  }

  // Error handling
  errorHandl(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
 }
}
