import { Component, OnInit } from '@angular/core';
import { bodega } from '../bodega';
import { FormGroup, FormBuilder } from '@angular/forms';
import { BodegasService } from '../bodegas.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-actualizarbodega',
  templateUrl: './actualizarbodega.component.html',
  styleUrls: ['./actualizarbodega.component.css']
})
export class ActualizarbodegaComponent implements OnInit {
  bod: bodega;
  submitted = false;
  bodegas: bodega[];
  selectedBodega = {};
  addForm: FormGroup;

  constructor(private bodservice: BodegasService, private router: Router, private formBuilder: FormBuilder) { 
    this.bod = JSON.parse( localStorage.getItem("bodegaEdit"));
  }

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      aula_id: this.bod.aula_id,
      aula_nombre: this.bod.aula_nombre,
      aula_edif_id: this.bod.aula_edif_id,
    });
  }

  get f() { return this.addForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.addForm.invalid) {
      return;
    }
    this.bodservice.editarporId(this.addForm.value, this.bod.aula_id)
      .subscribe(data => {
        alert("Bodega guardado con éxito");
        this.router.navigate(['/bodega/listar']);
      });
  }
}
