import { Component, OnInit } from '@angular/core';
import { Docente } from '../docente';
import { DocenteService} from '../docente.service';
import { Router } from '@angular/router';
import { QueryOptions } from 'src/app/spring-generic-mvc/model';

@Component({
  selector: 'app-listardocente',
  templateUrl: './listardocente.component.html',
  styleUrls: ['./listardocente.component.css']
})

export class ListardocenteComponent implements OnInit {

  //Listar docentes
  docentes = new Array<Docente>();
  parametro: string;
  public docenteEditar: Docente;
  selectedDocente: Docente[];
  
  bandera= false;
  cols: any[];
 
  loading = true;
  totalrecords;
  numerofilas = 20;
  pagina = 0;
  
  constructor(private docService: DocenteService, private router: Router) {  }

  ngOnInit() {
    this.cols = [
      { field: 'per_identificacion', header: 'Cédula'},
      { field: 'per_primernombre', header: 'Primer Nombre'},
      { field: 'per_segundonombre', header: 'Segundo Nombre'},
      { field: 'per_primerapellido', header: 'Apellido'},
      { field: 'per_celular', header: 'Celular'},
      { field: 'per_correo', header: 'Correo'},
    ];
    this.listar();
    this.selectedDocente=this.cols;
  }

  loadCarsLazy(event) {
    this.loading = true;

    // in a real application, make a remote request to load data using state metadata from event
    // event.first = First row offset
    // event.rows = Number of rows per page
    // event.sortField = Field name to sort with
    // event.sortOrder = Sort order as number, 1 for asc and -1 for dec
    // filters: FilterMetadata object having field as key and filter value, filter matchMode as value

    setTimeout(() => {
      if (this.docentes) {
        const q: QueryOptions = new QueryOptions(); //El archivo para paginar los articulos realizado en spring tools
        q.pageSize = event.rows;
        // Calculo de pagina
        let page = Math.ceil(event.first / this.numerofilas);
        this.pagina = page;
        if (page < 0) {
          page = 0;
        }
        q.pageNumber = page;
        // Sorts
        q.sortField = event.sortField;
        q.sortOrder = event.sortOrder;
        if (event.filters.global) {
          this.docService.find(q, event.filters.global.value).subscribe(resp => {
            this.docentes = resp.content;
            this.totalrecords = resp.totalElements*1;
          });
        } else {
          this.docService.list(q).subscribe(resp => {
            this.docentes = resp.content;
            this.totalrecords = resp.totalElements*1;
          });
        }

        this.loading = false;
      }
    }, 1000);
  }

  columnFilter(event: any) {
  }

  onRowSelect(e) {
  }

  listar() {
    const q: QueryOptions = new QueryOptions();
    q.pageSize = this.numerofilas;
    q.pageNumber = this.pagina;
    this.docService.list(q).subscribe(resp => {
      this.docentes = resp.content;
      this.totalrecords = resp.totalElements;
    });
  }

  listarDocente(){ // Funciona
    this.docService.listarDocente().subscribe(data =>{
      this.docentes = data;
    });
  }
  
  buscarporParametro(){ // Funciona
    this.docService.buscarporParametro(this.parametro).subscribe(data =>{
    this.docentes = data;
    });
  }

  volver(){
    this.bandera=false;
    this.listarDocente();
  }

}