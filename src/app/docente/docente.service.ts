import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Docente } from './docente';
import { retry, catchError } from 'rxjs/operators';

import { GenericService }from "src/app/spring-generic-mvc/service/generic.service";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DocenteService extends GenericService<Docente>{

  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor( private http: HttpClient ) { 
    super(
      http,
      environment.URL_DOCENTE,
      'docentes'
    )
  }

  // Listar ok
  listarDocente(): Observable<Docente[]>{
    return this.http.get<Docente[]>(`${environment.URL_DOCENTE}/docentes/listar`);
  }

  listarDoc(): Observable<Docente[]>{
    return this.http.get<Docente[]>(`${environment.URL_DOCENTE}/docentes/listardoc`);
  }
  

  // Buscar por parametro - ok
  buscarporParametro(parametro:string): Observable<Docente[]>{
    return this.http.get<Docente[]>(`${environment.URL_DOCENTE}/docentes/filter/${parametro}`);
  }

  // Error handling
  errorHandl(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
 }
    
}
