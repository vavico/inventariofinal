export interface Docente{
    per_identificacion: string;
    per_primerapellido: string;
    per_primernombre: string;
    per_segundonombre: string;
    per_celular: string;
    per_correo: string;
    per_fechanacimiento: string;
}