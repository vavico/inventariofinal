import { Component, OnInit } from '@angular/core';
import { categoria } from '../categoria';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CategoriasService } from '../categorias.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-actualizarcategoria',
  templateUrl: './actualizarcategoria.component.html',
  styleUrls: ['./actualizarcategoria.component.css']
})
export class ActualizarcategoriaComponent implements OnInit {
  cat: categoria;
  submitted = false;
  categorias: categoria[];
  selectedBodega = {};
  addForm: FormGroup;

  constructor(private catservice: CategoriasService, private router:Router, private formBuilder: FormBuilder) { 
    this.cat = JSON.parse( localStorage.getItem("categoriaEdit"));
  }

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      cat_id: this.cat.cat_id,
      cat_nombre: this.cat.cat_nombre,
      cat_tiempo_depre: this.cat.cat_tiempo_depre,
      cat_val_depre: this.cat.cat_val_depre,
    });
  }

  get f() { return this.addForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.addForm.invalid) {
      return;
    }
    this.catservice.editarporId(this.addForm.value, this.cat.cat_id)
      .subscribe(data => {
        alert("Categoria guardado con éxito");
        this.router.navigate(['/categoria/listar']);
      });
  }
}
