import { Component, OnInit } from '@angular/core';
import { categoria } from '../categoria';
import { CategoriasService } from '../categorias.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listarcategoria',
  templateUrl: './listarcategoria.component.html',
  styleUrls: ['./listarcategoria.component.css']
})
export class ListarcategoriaComponent implements OnInit {

   //Listar categoria
   categorias = new Array<categoria>();
   nombre: string;
   public catEditar: categoria;
   bandera= false;
   cols: any[];
   selectedCat: categoria;
   
   constructor(private catService: CategoriasService, private router: Router) {  }
 
   ngOnInit() {
     this.listarCategoria();
     
     this.cols =[
       { field: 'cat_id', header: 'ID'},
       { field: 'cat_nombre', header: 'Nombre'},
       { field: 'cat_val_depre', header: 'Valor Depreciación'},
       { field: 'cat_tiempo_depre', header: 'Tiempo Depre (años)'},
     ];
   }
 
   listarCategoria(){ // Funciona
     this.catService.listarCategoria().subscribe(data =>{
       this.categorias = data;
     });
   }
   
   buscarporNombre(){ // Funciona
     this.catService.buscarporNombre(this.nombre).subscribe(data =>{
     this.categorias = data;
     });
   }
 
   editarporId(){ 
    localStorage.setItem('categoriaEdit',JSON.stringify(this.selectedCat));
    this.router.navigate(['/categoria/editar']); 
    this.bandera=true;
   }
 
   eliminarporId(cat: categoria){ // Funciona
      this.catService.eliminarporId(cat.cat_id).subscribe(data =>{
        alert("Eliminado");
        this.listarCategoria();
       });
     
   }
 
   volver(){
     this.bandera=false;
     this.listarCategoria();
   }
}
