import { Component, OnInit } from '@angular/core';
import { CategoriasService } from '../categorias.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-registracategoria',
  templateUrl: './registracategoria.component.html',
  styleUrls: ['./registracategoria.component.css']
})
export class RegistracategoriaComponent implements OnInit {

  constructor(private catservice: CategoriasService, private router: Router, private formBuilder: FormBuilder) { }
  addForm: FormGroup;
  submitted = false;

  ngOnInit() {
    
    this.addForm = this.formBuilder.group({
      cat_id : [],
      cat_nombre : ['',Validators.required],
      cat_val_depre : ['',Validators.required],
      cat_tiempo_depre : ['',Validators.required]
    });
  }
  get f(){return this.addForm.controls;}

  onSubmit(){
    this.submitted = true;
    if (this.addForm.invalid){
      return;
    }
    this.catservice.crearCategoria(this.addForm.value)
    .subscribe(data => {
      alert("Artículo guardado con éxito");
      this.router.navigate(['/categoria/listar']);
    });

  }

}
