import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { categoria } from './categoria';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoriasService {

  private url: string = environment.URL_INVENTARIO + "/categoria";

  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor( private http: HttpClient ) { }

  // Guardar articulo - ok
  crearCategoria(cat: categoria): Observable<categoria>{
    return this.http.post<categoria>(`${this.url}/guardar`,cat);
  }

  // Listar articulos - ok
  listarCategoria(): Observable<categoria[]>{
    return this.http.get<categoria[]>(`${this.url}/listar`);
  }


  editarporId(data: categoria, id): Observable<categoria> {
    return this.http.put<categoria>(`${this.url}/editar/${id}`, JSON.stringify(data), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    )
  }

  // Eliminar por id - ok
  eliminarporId(id:number) {
    return this.http.delete(`${this.url}/eliminar/${id}`);
  }

  // Buscar por nombre - ok
  buscarporNombre(nombre:string): Observable<categoria[]>{
    return this.http.get<categoria[]>(`${this.url}/listar/${nombre}`);
  }

  // Error handling
  errorHandl(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
 }
}
