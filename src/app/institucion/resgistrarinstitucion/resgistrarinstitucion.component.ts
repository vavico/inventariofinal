import { Component, OnInit } from '@angular/core';
import { InstitucionService } from '../institucion.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-resgistrarinstitucion',
  templateUrl: './resgistrarinstitucion.component.html',
  styleUrls: ['./resgistrarinstitucion.component.css']
})
export class ResgistrarinstitucionComponent implements OnInit {

  constructor(private empservice: InstitucionService, private router: Router, private formBuilder: FormBuilder) { }
  
  addForm: FormGroup;
  submitted = false;
  
  ngOnInit() {
  
    this.addForm = this.formBuilder.group({
      emp_id : [],
      emp_nombre : ['',Validators.required],
    });
  }
  get f(){return this.addForm.controls;}

  onSubmit(){
    this.submitted = true;
    if (this.addForm.invalid){
      return;
    }
    this.empservice.crearEmpresa(this.addForm.value).subscribe(data => {
      alert("Empresa guardado con éxito");
      this.router.navigate(['/institucion/listar']);
    });

  }
}
