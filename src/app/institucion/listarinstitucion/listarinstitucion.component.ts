import { Component, OnInit } from '@angular/core';
import { institucion } from '../institucion';
import { InstitucionService } from '../institucion.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listarinstitucion',
  templateUrl: './listarinstitucion.component.html',
  styleUrls: ['./listarinstitucion.component.css']
})
export class ListarinstitucionComponent implements OnInit {

  //Listar empresas
  institucion = new Array<institucion>();
  nombre: string;
  bandera = false;
  empEditar: institucion;
  cols: any[];
  selectedInsti: institucion;

  constructor(private empService: InstitucionService, private router: Router) { }

  ngOnInit() {
    this.listarInstitucion();

    this.cols=[
      {field: "emp_id", header: "ID"},
      {field: "emp_nombre", header: "Nombre"},
    ];
  }

  listarInstitucion(){ // Funciona
    this.empService.listarEmpresa().subscribe(data =>{
      this.institucion = data;
    });
  }
  
  buscarporNombre(){ // Funciona
    this.empService.buscarporNombre(this.nombre).subscribe(data =>{
    this.institucion = data;
    });
  }

  editarporId(){
    localStorage.setItem('institucionEdit',JSON.stringify(this.selectedInsti));
    this.router.navigate(['/institucion/editar']);
    this.bandera=true;
  }

  eliminarporId(emp: institucion){ // Funciona
     this.empService.eliminarporId(emp.emp_id).subscribe(data =>{
       alert("Eliminado");
       this.listarInstitucion();
      });
  }

  volver(){
    this.bandera=false;
    this.listarInstitucion();
  }

}
