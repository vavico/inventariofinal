import { Component, OnInit } from '@angular/core';
import { institucion } from '../institucion';
import { FormGroup, FormBuilder } from '@angular/forms';
import { InstitucionService } from '../institucion.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-actualizarinstitucion',
  templateUrl: './actualizarinstitucion.component.html',
  styleUrls: ['./actualizarinstitucion.component.css']
})
export class ActualizarinstitucionComponent implements OnInit {
  emp: institucion;
  submitted= false;
  empresas: institucion[];
  selectedEmpresa = {}
  addForm: FormGroup;

  constructor(private empservice: InstitucionService, private router: Router, private formBuilder: FormBuilder) { 
    this.emp = JSON.parse( localStorage.getItem("institucionEdit"));
  }

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      emp_id: this.emp.emp_id,
      emp_nombre: this.emp.emp_nombre,
    });
  }

  get f() { return this.addForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.addForm.invalid) {
      return;
    }
    this.empservice.editarporId(this.addForm.value, this.emp.emp_id)
      .subscribe(data => {
        alert("Empresa guardado con éxito");
        this.router.navigate(['/institucion/listar']);
      });
  }

}
