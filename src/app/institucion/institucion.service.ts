import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { institucion } from './institucion';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InstitucionService {

  private url: string = environment.URL_INVENTARIO + "/empresa";
  
  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor( private http: HttpClient) { }

  // Guardar EMPRESA - ok
  crearEmpresa(emp: institucion): Observable<institucion>{
    return this.http.post<institucion>(`${this.url}/guardar`, emp);
  }

  // Listar articulos - ok
  listarEmpresa(): Observable<institucion[]>{
    return this.http.get<institucion[]>(`${this.url}/listar`);
  }


  editarporId(data: institucion, id): Observable<institucion> {
    return this.http.put<institucion>(`${this.url}/editar/${id}`, JSON.stringify(data), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    )
  }

  // Eliminar por id - ok
  eliminarporId(id:number) {
    return this.http.delete(`${this.url}/eliminar/${id}`);
  }

  // Buscar por nombre - ok
  buscarporNombre(nombre:string): Observable<institucion[]>{
    return this.http.get<institucion[]>(`${this.url}/listar/${nombre}`);
  }

  // Error handling
  errorHandl(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
 }
}
