import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ListardocenteComponent } from './docente/listardocente/listardocente.component';
import { ListarinventarioComponent } from './inventario/listarinventario/listarinventario.component';
import { RegistrarinventarioComponent } from './inventario/registrarinventario/registrarinventario.component';

import { ListararticuloComponent } from './articulo/listararticulo/listararticulo.component';
import { RegistrararticuloComponent } from './articulo/registrararticulo/registrararticulo.component';
import { ActualizararticuloComponent } from './articulo/actualizararticulo/actualizararticulo.component';

import { MegaMenuModule } from 'primeng/megamenu';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { HttpClientModule } from '@angular/common/http';

import { DocenteService } from './docente/docente.service';
import { ArticuloService } from './articulo/articulo.service';
import { InventarioService } from './inventario/inventario.service';

import { PDFExportModule } from '@progress/kendo-angular-pdf-export';

import { MenuComponent } from './menu/menu.component';
import { LoginComponent } from './login/login.component';
import { LoginService } from './login/login.service';
import { UsuarioService } from './login/usuario.service';
import { PanelModule } from 'primeng/panel';

import { RegistracategoriaComponent } from './categoria/registracategoria/registracategoria.component';
import { ListarcategoriaComponent } from './categoria/listarcategoria/listarcategoria.component';
import { ActualizarcategoriaComponent } from './categoria/actualizarcategoria/actualizarcategoria.component';
import { CategoriasService } from './categoria/categorias.service';

import { RegistrarbodegaComponent } from './bodega/registrarbodega/registrarbodega.component';
import { ListarbodegaComponent } from './bodega/listarbodega/listarbodega.component';
import { ActualizarbodegaComponent } from './bodega/actualizarbodega/actualizarbodega.component';

import { ResgistrarinstitucionComponent } from './institucion/resgistrarinstitucion/resgistrarinstitucion.component';
import { ListarinstitucionComponent } from './institucion/listarinstitucion/listarinstitucion.component';

import {LocationStrategy, HashLocationStrategy} from '@angular/common';

import { AccordionModule } from 'primeng/accordion';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { CalendarModule } from 'primeng/calendar';
import { CardModule } from 'primeng/card';
import { CarouselModule } from 'primeng/carousel';
import { ChartModule } from 'primeng/chart';
import { CheckboxModule } from 'primeng/checkbox';
import { ChipsModule } from 'primeng/chips';
import { CodeHighlighterModule } from 'primeng/codehighlighter';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ColorPickerModule } from 'primeng/colorpicker';
import { ContextMenuModule } from 'primeng/contextmenu';
import { DataViewModule } from 'primeng/dataview';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { EditorModule } from 'primeng/editor';
import { FieldsetModule } from 'primeng/fieldset';
import { FileUploadModule } from 'primeng/fileupload';
import { FullCalendarModule } from 'primeng/fullcalendar';
import { GalleriaModule } from 'primeng/galleria';
import { GrowlModule } from 'primeng/growl';
import { InplaceModule } from 'primeng/inplace';
import { InputMaskModule } from 'primeng/inputmask';
import { InputSwitchModule } from 'primeng/inputswitch';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { LightboxModule } from 'primeng/lightbox';
import { ListboxModule } from 'primeng/listbox';
import { MenuModule } from 'primeng/menu';
import { MenubarModule } from 'primeng/menubar';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { MultiSelectModule } from 'primeng/multiselect';
import { OrderListModule } from 'primeng/orderlist';
import { OrganizationChartModule } from 'primeng/organizationchart';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { PaginatorModule } from 'primeng/paginator';
import { PanelMenuModule } from 'primeng/panelmenu';
import { PasswordModule } from 'primeng/password';
import { PickListModule } from 'primeng/picklist';
import { ProgressBarModule } from 'primeng/progressbar';
import { RadioButtonModule } from 'primeng/radiobutton';
import { RatingModule } from 'primeng/rating';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { SelectButtonModule } from 'primeng/selectbutton';
import { SlideMenuModule } from 'primeng/slidemenu';
import { SliderModule } from 'primeng/slider';
import { SpinnerModule } from 'primeng/spinner';
import { SplitButtonModule } from 'primeng/splitbutton';
import { StepsModule } from 'primeng/steps';
import { TabMenuModule } from 'primeng/tabmenu';
import { TabViewModule } from 'primeng/tabview';
import { TerminalModule } from 'primeng/terminal';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { ToastModule } from 'primeng/toast';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { ToolbarModule } from 'primeng/toolbar';
import { TooltipModule } from 'primeng/tooltip';
import { TreeModule } from 'primeng/tree';
import { TreeTableModule } from 'primeng/treetable';
import { VirtualScrollerModule } from 'primeng/virtualscroller';

import {AppMenuComponent, AppSubMenuComponent} from './app.menu.component';
import {DashboardDemoComponent} from './demo/view/dashboarddemo.component';
import {SampleDemoComponent} from './demo/view/sampledemo.component';
import {FormsDemoComponent} from './demo/view/formsdemo.component';
import {DataDemoComponent} from './demo/view/datademo.component';
import {PanelsDemoComponent} from './demo/view/panelsdemo.component';
import {OverlaysDemoComponent} from './demo/view/overlaysdemo.component';
import {MenusDemoComponent} from './demo/view/menusdemo.component';
import {MessagesDemoComponent} from './demo/view/messagesdemo.component';
import {MiscDemoComponent} from './demo/view/miscdemo.component';
import {EmptyDemoComponent} from './demo/view/emptydemo.component';
import {ChartsDemoComponent} from './demo/view/chartsdemo.component';
import {FileDemoComponent} from './demo/view/filedemo.component';
import {UtilsDemoComponent} from './demo/view/utilsdemo.component';
import {DocumentationComponent} from './demo/view/documentation.component';

import {CarService} from './demo/service/carservice';
import {CountryService} from './demo/service/countryservice';
import {EventService} from './demo/service/eventservice';
import {NodeService} from './demo/service/nodeservice';
import { ModuloComponent } from './Inicio/modulo/modulo.component';
import { DepreciacionComponent } from './articulo/depreciacion/depreciacion.component';
import { VistaComponent } from './soporte/vista/vista.component';

import { SpringGenericMVCModule } from './spring-generic-mvc/SpringGenericMVCModule';
import { InstitucionService } from './institucion/institucion.service';
import { ActualizarinstitucionComponent } from './institucion/actualizarinstitucion/actualizarinstitucion.component';
import { MatCheckboxModule } from '@angular/material';
import { RepoarticulosComponent } from './Reportes/repoarticulos/repoarticulos.component';
import { RegistrarasignacionComponent } from './asignar/registrarasignacion/registrarasignacion.component';
import { ListarasignacionComponent } from './asignar/listarasignacion/listarasignacion.component';
import { ModificarasignacionComponent } from './asignar/modificarasignacion/modificarasignacion.component';

@NgModule({
  declarations: [

    AppComponent,
    ListardocenteComponent,
    ListarinventarioComponent,
    RegistrarinventarioComponent,
    ListararticuloComponent,
    RegistrararticuloComponent,
    ActualizararticuloComponent,
    MenuComponent,
    LoginComponent,
    RegistracategoriaComponent,
    ListarcategoriaComponent,
    ActualizarcategoriaComponent,
    RegistrarbodegaComponent,
    ListarbodegaComponent,
    ActualizarbodegaComponent,

    AppMenuComponent,
    AppSubMenuComponent,
    DashboardDemoComponent,
    SampleDemoComponent,
    FormsDemoComponent,
    DataDemoComponent,
    PanelsDemoComponent,
    OverlaysDemoComponent,
    MenusDemoComponent,
    MessagesDemoComponent,
    MessagesDemoComponent,
    MiscDemoComponent,
    ChartsDemoComponent,
    EmptyDemoComponent,
    FileDemoComponent,
    UtilsDemoComponent,
    DocumentationComponent,
    ModuloComponent,
    DepreciacionComponent,
    VistaComponent,
    ResgistrarinstitucionComponent,
    ListarinstitucionComponent,
    ActualizarinstitucionComponent,
    RepoarticulosComponent,
    RegistrarasignacionComponent,
    ListarasignacionComponent,
    ModificarasignacionComponent
  ],
  imports: [

    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MegaMenuModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    TableModule,
    HttpClientModule,
    PDFExportModule,
    PanelModule,
    AccordionModule,
    AutoCompleteModule,
    BreadcrumbModule,
    ButtonModule,
    CalendarModule,
    CardModule,
    CarouselModule,
    ChartModule,
    CheckboxModule,
    ChipsModule,
    CodeHighlighterModule,
    ConfirmDialogModule,
    ColorPickerModule,
    ContextMenuModule,
    DataViewModule,
    DialogModule,
    DropdownModule,
    EditorModule,
    FieldsetModule,
    FileUploadModule,
    FullCalendarModule,
    GalleriaModule,
    GrowlModule,
    InplaceModule,
    InputMaskModule,
    InputSwitchModule,
    InputTextModule,
    InputTextareaModule,
    LightboxModule,
    ListboxModule,
    MegaMenuModule,
    MenuModule,
    MenubarModule,
    MessageModule,
    MessagesModule,
    MultiSelectModule,
    OrderListModule,
    OrganizationChartModule,
    OverlayPanelModule,
    PaginatorModule,
    PanelModule,
    PanelMenuModule,
    PasswordModule,
    PickListModule,
    ProgressBarModule,
    RadioButtonModule,
    RatingModule,
    ScrollPanelModule,
    SelectButtonModule,
    SlideMenuModule,
    SliderModule,
    SpinnerModule,
    SplitButtonModule,
    StepsModule,
    TableModule,
    TabMenuModule,
    TabViewModule,
    TerminalModule,
    TieredMenuModule,
    ToastModule,
    ToggleButtonModule,
    ToolbarModule,
    TooltipModule,
    TreeModule,
    TreeTableModule,
    VirtualScrollerModule,
    SpringGenericMVCModule,
    MatCheckboxModule

  ],
  providers: [DocenteService,ArticuloService,InventarioService, LoginService, UsuarioService,CategoriasService, InstitucionService,
              {provide: LocationStrategy, useClass: HashLocationStrategy},
              CarService, CountryService, EventService, NodeService],
  bootstrap: [AppComponent]
  
})
export class AppModule { }
